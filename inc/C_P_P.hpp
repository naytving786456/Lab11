#pragma once
#include <Code.hpp>

class C_Plus_Plus : public Code
{
public:
	
	C_Plus_Plus(Language language) : Code(language)
	{
		cout << "C_P_P class : constructor" << endl;
	}

	virtual ~C_Plus_Plus()
	{
		cout << "C_P_P class: destructor" << endl;
	}

	string SomeCode() override
	{
		return "C_P_P code";
	}
};