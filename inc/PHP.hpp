#pragma once
#include <Code.hpp>

class PhP : public Code
{
public:

	PhP(Language language) : Code(language)
	{
		cout << "PHP class : constructor" << endl;
	}

	virtual ~PhP()
	{
		cout << "PHP class: destructor" << endl;
	}

	string SomeCode() override
	{
		return "PHP code";
	}
};