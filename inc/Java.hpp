#pragma once
#include <Code.hpp>

class JAVA : public Code
{
public:

	JAVA(Language language) : Code(language)
	{
		cout << "Java class : constructor" << endl;
	}

	virtual ~JAVA()
	{
		cout << "Java class: destructor" << endl;
	}

	string SomeCode() override
	{
		return "Java code";
	}
};