#pragma once
#include <iostream>
#include <PHP.hpp>
#include <Java.hpp>
#include <C_P_P.hpp>
#include <Code.hpp>

Code* genCode(Language language)
{
	Code* shift = nullptr;

	switch (language)
{
	case Java:
    {
        shift = new  JAVA(language);
        shift->SomeCode();
        return shift;
        break;
    }
    case C_P_P:
    {
        shift = new C_Plus_Plus(language);
        shift->SomeCode();
        return shift;
        break;
    }
    case PHP:
    {
        shift = new PhP(language);
        shift->SomeCode();
        return shift;
        break;
    }
}
}