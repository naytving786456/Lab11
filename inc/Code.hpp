#pragma once
#include <iostream>

using namespace std;

enum Language { Java = 0, C_P_P = 1 , PHP = 2};

class Code
{
public:
	
	Code(Language language)
	{
		m_language = language;
		cout << "Base class: constuctor" << endl;
	}

	virtual ~Code()
	{
		cout << "Base class: destructor" << endl;
	}

	virtual string SomeCode() = 0;

protected:
	Language m_language;
};