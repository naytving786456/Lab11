cmake_minimum_required(VERSION 3.15)
set(PROJECT_NAME Lab11)
project("${PROJECT_NAME}")
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)


include_directories("inc/")

add_executable("${PROJECT_NAME}" main.cpp inc/Functions.hpp inc/Code.hpp inc/Java.hpp inc/PHP.hpp inc/C_P_P.hpp)