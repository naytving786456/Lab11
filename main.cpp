﻿#include <iostream>
#include <PHP.hpp>
#include <Java.hpp>
#include <C_P_P.hpp>
#include <Code.hpp>
#include <Functions.hpp>

using namespace std;

int main()
{
    cout << "  ====   java   ====  " << endl;
    Code* code = genCode(Java);
    cout << code->SomeCode() << endl;

    cout << "  ====   php   ====  " << endl;

    Code* code1 = genCode(PHP);
    cout << code1->SomeCode() << endl;

    cout << "  ====   c_plus_plus   ====  " << endl;

    Code* code2 = genCode(C_P_P);
    cout << code2->SomeCode() << endl;

    delete code;
    delete code1;
    delete code2;




    return 0;
}